import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { WsException } from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { Socket } from 'socket.io';
import appConfig from 'src/config/app.config';
import { AuthService } from '../auth/auth.service';
import { User } from '../user/entities/user.entity';
import { UserService } from '../user/user.service';
import { Reflector } from '@nestjs/core';

@Injectable()
export class WsJwtGuard implements CanActivate {
  private logger: Logger = new Logger(WsJwtGuard.name);

  constructor(private reflector: Reflector, private userService: UserService) {}

  canActivate(
    context: any,
  ): boolean | any | Promise<boolean | any> | Observable<boolean | any> {
    const bearerToken =
      context.args[0].handshake.headers.authorization.split(' ')[1];
    try {
      const roles = this.reflector.get<string[]>('roles', context.getHandler());
      const decoded = jwt.verify(bearerToken, appConfig().appSecret) as any;
      return new Promise((resolve, reject) => {
        return this.userService.getUserById(decoded.sub).then((user) => {
          if (user) {
            if (roles.includes(user.role)) {
              resolve(user);
            } else {
              reject(false);
            }
          } else {
            reject(false);
          }
        });
      });
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }
}
