import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiParam,
  ApiProperty,
  ApiTags,
} from '@nestjs/swagger';
import { SETTINGS } from 'src/app.utils';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

import { UserRegisterRequestDto } from './dto/user-register.req.dto';
import { UserUpdatePasswordRequestDto } from './dto/user-update-password.req.dto';
import { UserUpdateRequestDto } from './dto/user-update.req.dto';
import { User } from './entities/user.entity';
import { UserRoles } from './enums/user.enum';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('api')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('/users')
  @ApiCreatedResponse({
    description: 'Created user object as response',
    type: User,
  })
  @ApiBadRequestResponse({ description: 'User cannot register. Try again!' })
  async doUserRegistration(
    @Body(SETTINGS.VALIDATION_PIPE)
    userRegister: UserRegisterRequestDto,
  ): Promise<User> {
    return await this.userService.doUserRegistration(userRegister);
  }

  @Get('users')
  async getUsers() {
    return await this.userService.getUsers();
  }

  @ApiParam({
    name: 'id',
    description: 'Get User by id',
  })
  @Get('users/:id')
  async getUser(@Request() req, @Param() params): Promise<User> {
    console.log(params.id);

    return await this.userService.getUserById(params.id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRoles.ADMIN)
  @ApiParam({
    name: 'id',
    type: 'number',
    description: 'Delete User by id',
  })
  @Delete('users/:id')
  async deleteUser(@Request() req, @Param() params) {
    return await this.userService.deleteUser(params.id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiParam({
    name: 'id',
    type: 'number',
    description: 'Update User by id',
  })
  @Patch('users/:id')
  async updateUser(
    @Param('id') id,
    @Body(SETTINGS.VALIDATION_PIPE)
    userUpdate: UserUpdateRequestDto,
  ) {
    return await this.userService.updateUser(id, userUpdate);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put('password')
  async updatePasswordUser(
    @Request() req,
    @Body(SETTINGS.VALIDATION_PIPE)
    userUpdatePassword: UserUpdatePasswordRequestDto,
  ) {
    const id = req.user.id;
    return await this.userService.updatePassword(id, userUpdatePassword);
  }
}
