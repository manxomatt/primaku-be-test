import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRegisterRequestDto } from './dto/user-register.req.dto';
import { UserUpdatePasswordRequestDto } from './dto/user-update-password.req.dto';
import { UserUpdateRequestDto } from './dto/user-update.req.dto';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  async doUserRegistration(
    userRegister: UserRegisterRequestDto,
  ): Promise<User> {
    const user = new User();
    user.name = userRegister.name;
    user.email = userRegister.email;
    user.password = userRegister.password;
    user.role = userRegister.role;

    return await user.save();
  }

  public async getUserByEmail(email: string): Promise<User | undefined> {
    return User.findOne({ where: { email } });
  }

  async getUserById(id: number): Promise<User | undefined> {
    return User.findOne({
      select: {
        id: true,
        name: true,
        email: true,
        role: true,
        createdAt: true,
        updatedAt: true,
      },
      where: { id },
    });
  }

  public async getUsers(): Promise<User[]> {
    return User.find({
      select: {
        id: true,
        name: true,
        email: true,
        role: true,
        createdAt: true,
        updatedAt: true,
      },
    });
  }

  public async deleteUser(id: number) {
    return User.delete(id);
  }

  public async updateUser(id: number, userUpdate: UserUpdateRequestDto) {
    const user = await User.findOne({
      where: {
        id,
      },
    });

    if (user != null) {
      user.name = userUpdate.name;
      user.email = userUpdate.email;
      user.role = userUpdate.role;

      return user.save();
    } else {
      return null;
    }
  }

  public async updatePassword(
    id: number,
    userUpdatePassword: UserUpdatePasswordRequestDto,
  ) {
    const user = await User.findOne({
      where: {
        id,
      },
    });

    if (user != null) {
      // console.log(userUpdatePassword.oldPassword);
      if (
        !(await bcrypt.compare(userUpdatePassword.oldPassword, user.password))
      )
        throw new UnauthorizedException();

      const salt = await bcrypt.genSalt();
      user.password = await bcrypt.hash(userUpdatePassword.password, salt);

      return user.save();
    } else {
      return null;
    }
  }
}
