import { UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Server } from 'socket.io';
import { Roles } from 'src/modules/auth/roles.decorator';
import { RolesGuard } from 'src/modules/auth/roles.guard';
import { WsJwtGuard } from 'src/modules/auth/ws-jwt-guard';
import { User } from 'src/modules/user/entities/user.entity';
import { UserRoles } from 'src/modules/user/enums/user.enum';
import { UserService } from 'src/modules/user/user.service';

@WebSocketGateway({
  // namespace: 'events',
  cors: {
    origin: '*',
  },
})
export class EventGateway {
  constructor(private userService: UserService) {}

  @WebSocketServer()
  server: Server;

  // @SubscribeMessage('events')
  // findAll(@MessageBody() data: any): Observable<WsResponse<number>> {
  //   console.log('events call');
  //   return from([1, 2, 3]).pipe(
  //     map((item) => ({ event: 'events', data: item })),
  //   );
  // }
  // @SubscribeMessage('identity')
  // async identity(@MessageBody() data: number): Promise<number> {
  //   console.log('Identity');
  //   return data;
  // }

  @UseGuards(WsJwtGuard)
  @Roles(UserRoles.ADMIN)
  @SubscribeMessage('eventMemberUsers')
  async members(): Promise<User[]> {
    // console.log('MEMBER USERS');
    const users = await this.userService.getUsers();
    return users;
  }
}
