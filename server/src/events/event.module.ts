import { Module } from '@nestjs/common';
import { UserService } from 'src/modules/user/user.service';
import { EventGateway } from './event.gateway';

@Module({
  providers: [EventGateway, UserService],
})
export class EventModule {}
