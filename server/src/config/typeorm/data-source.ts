// import { RoleEntity } from '../../../src/modules/user/entities/role.entity';
import { User as UserEntity } from '../../../src/modules/user/entities/user.entity';
import { DataSource } from 'typeorm';
import { UserCreateSeed } from 'src/database/seeds/user-create.seed';

const AppDataSource = new DataSource({
  type: 'mysql',
  host: 'db',
  port: 3306,
  username: 'root',
  password: 'password',
  database: 'primaku',
  synchronize: false,
  logging: true,
  entities: [UserEntity],
  migrations: ['src/database/migrations/*.ts'],
  subscribers: [],
});

export default AppDataSource;
