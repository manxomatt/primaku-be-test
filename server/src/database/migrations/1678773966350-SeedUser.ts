import { MigrationInterface, QueryRunner } from 'typeorm';

export class SeedUser1678773966350 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO \`users\` (\`name\`, \`email\`, \`password\`, \`role\`) values('Admin','admin@primaku.com','$2b$10$FIREYBe23qXNA/6o4dAz6enBUsORvK3piVg6AVHTzWxaK4SPkTfrq','admin')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM \`users\``);
  }
}
