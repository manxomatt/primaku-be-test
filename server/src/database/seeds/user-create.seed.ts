// import { Factory, Seeder } from 'typeorm-seeding';
import { UserRoles } from '../../modules/user/enums/user.enum';
import { User } from '../../modules/user/entities/user.entity';
import { Factory, Seeder } from '@concepta/typeorm-seeding';
import { UserFactory } from '../factories/user.factory';

export class UserCreateSeed extends Seeder {
  async run() {
    this.factory(UserFactory).create();
  }
}
