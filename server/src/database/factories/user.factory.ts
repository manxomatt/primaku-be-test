import { Factory } from '@concepta/typeorm-seeding';
import { randEmail, randFullName, randPassword } from '@ngneat/falso';
import { UserRoles } from 'src/modules/user/enums/user.enum';
// import { define } from 'typeorm-seeding';
import { User } from '../../modules/user/entities/user.entity';

export class UserFactory extends Factory<User> {
  protected async entity(): Promise<User> {
    const user = new User();
    user.name = 'Admin';
    user.email = 'admin@primaku.com';
    user.password = '#Admin123';
    user.role = UserRoles.ADMIN;
    return user;
  }
}
